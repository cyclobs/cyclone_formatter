from .sarReader import readSar
from .smosReader import readSmos
from .smapReader import readSmap
import logging
import os

logging.basicConfig()
logger = logging.getLogger(os.path.basename(__file__))
logger.setLevel(logging.WARNING)


def readFormatted(path, type='auto'):
    if type == "auto":
        # TODO Use acquisition mission string to autodetect
        if "SMOS" in path or "smos" in path:
            return readSmos(path)
        elif "owi" in path:
            return readSar(path)
        elif "smap" in path:
            return readSmap(path)
        else:
            logger.error(f"Couldn't find the proper reader for this path : {path}")
    elif type == "SAR":
        return readSar(path)
    elif type == "SMOS":
        return readSmos(path)
    elif type == "SMAP":
        return readSmap(path)


