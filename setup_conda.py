from setuptools import setup, find_packages
import glob

setup(name='cyclone_formatter',
      description='Read files containing geo-located wind data and format it to a generic data structure',
      url='https://gitlab.ifremer.fr/cyclobs/cyclone_formatter',
      author="Théo CEVAER",
      author_email="theo.cevaer@ifremer.fr",
      license='GPL',
      packages=find_packages(),
      use_scm_version=True,
      setup_requires=['setuptools_scm'],
      include_package_data=True,
      zip_safe=False,
      #install_requires=['xarray', 'h5py', 'owi>=0.0.dev0', 'pandas']
      )
