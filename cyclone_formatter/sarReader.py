import xarray as xr
import owi
import numpy as np


def setNrcsAttrs(ds, name):
    if name not in ds.data_vars:
        pass
    else:
        ds[name].attrs["cmap"] = "gray"

        varNoLand = ds[name].values[ds["mask_flag"].values == 0]
        min = varNoLand.min()
        max = varNoLand.max()
        max = np.percentile(varNoLand, 99.5)

        ds[name].attrs["clim_min"] = min
        ds[name].attrs["clim_max"] = max


def readSar(path):
    if "_gd" in path:
        ds = xr.open_dataset(path).squeeze()
        ds["wind_speed"].attrs["cmap"] = "high_wind_speed"
        ds["wind_speed"].attrs["clim_min"] = 0
        ds["wind_speed"].attrs["clim_max"] = 80
        setNrcsAttrs(ds, "nrcs_cross")
        setNrcsAttrs(ds, "nrcs_co")
        setNrcsAttrs(ds, "nrcs_detrend_cross")
        setNrcsAttrs(ds, "nrcs_detrend_co")

        # Define those for correct plotting of wind_streaks_orientation as vector field with
        # wind_streaks_orienation_sttdev as background
        ds["wind_streaks_orientation_stddev"].attrs["cmap"] = "PiYG_r"
        ds["wind_streaks_orientation_stddev"].attrs["clim_min"] = 0
        ds["wind_streaks_orientation_stddev"].attrs["clim_max"] = 80

        # Compute the angle, which can be used to plot a vector field
        mag = np.ones(ds.wind_speed.values.shape) * 1
        u = np.cos(np.deg2rad(360 - ds["wind_streaks_orientation"].values - 90))  # zonal
        v = np.sin(np.deg2rad(360 - ds["wind_streaks_orientation"].values - 90))  # merid
        angle = (np.pi / 2.) - np.arctan2(u / mag, v / mag)
        ds["angle"] = (["lat", "lon"], angle)
    elif "_sw" in path:
        ds = xr.open_dataset(path).transpose().squeeze()
        ds["wind_speed"].attrs["cmap"] = "high_wind_speed"
        ds["nrcs_cross"].attrs["cmap"] = "gray"
        ds["nrcs_detrend_co"].attrs["cmap"] = "gray"
        ds["nrcs_detrend_cross"].attrs["cmap"] = "gray"
    else:
        nc = owi.readFile(path)
        ds = xr.Dataset({"wind_speed": (['x', 'y'], nc["owiInversionTables_UV/owiWindSpeed_Tab_dualpol_2steps"]),
                         "nice_display": (['x', 'y'], nc["owiPreProcessing/ND_2"])},
                        coords={'lon': (['x', 'y'], nc["owiLon"]),
                                'lat': (['x', 'y'], nc["owiLat"])})
        ds["wind_speed"].attrs["cmap"] = "high_wind_speed"

    return ds

