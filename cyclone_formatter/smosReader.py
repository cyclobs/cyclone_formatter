import xarray as xr
import h5py
import numpy as np


def deg180(deg360):
    """
    convert [0,360] to [-180,180]
    """
    res = np.copy(deg360)
    res[res > 180] = res[res > 180]-360
    return res


def readSmos(path):
    # swath type
    if "SMOS" in path:
        with h5py.File(path, 'r') as f:
            ds = xr.Dataset({"wind_speed": (['x', 'y'], f['cart_grid']['rws10_mps'])},
                            coords={'lon': (['x', 'y'], deg180(f['cart_grid']['lon'])),
                                    'lat': (['x', 'y'], f['cart_grid']['lat'])})
            ds["wind_speed"].attrs["cmap"] = "high_wind_speed"
            return ds
    elif "SM" in path:
        ds = xr.open_dataset(path)
        ds["wind_speed"].attrs["cmap"] = "high_wind_speed"
        ds["wind_speed"].attrs["clim_min"] = 0
        ds["wind_speed"].attrs["clim_max"] = 80
        # ds = ds.rename_dims({"lon": "x", "lat": "y"})
        return ds
