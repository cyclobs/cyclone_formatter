import xarray as xr


def readSmap(path):
    ds = xr.open_dataset(path)
    ds["wind_speed"].attrs["cmap"] = "high_wind_speed"
    ds["wind_speed"].attrs["clim_min"] = 0
    ds["wind_speed"].attrs["clim_max"] = 80
   # ds = ds.rename_dims({"lon": "x", "lat": "y"})

    return ds
